LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY Tarea7 IS
	PORT( CLOCK_50 : IN std_logic;
			HEX0 : OUT std_logic_vector(6 downto 0));
END Tarea7;

ARCHITECTURE Structure OF Tarea7 IS
COMPONENT driver7Segmentos IS
	PORT( codigoNum : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			bitsNum : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END COMPONENT;			
	SIGNAL cont : std_logic_vector(24 downto 0) := (others => '0');
	SIGNAL clock1Hz : std_logic_vector(3 downto 0) := (others => '0');
BEGIN
	Visor : driver7Segmentos
		Port Map( codigoNum => clock1Hz,
					 bitsNum => HEX0);
					 
	PROCESS (CLOCK_50)
	BEGIN
		if rising_edge(CLOCK_50) then
			cont <= cont + '1';
		end if;
	END PROCESS;
	
	PROCESS (cont(24))
	BEGIN
		if rising_edge(cont(24)) then
			if clock1Hz = "1001" then
				clock1Hz <= "0000";
			else
				clock1Hz <= clock1Hz + '1';
			end if;
		end if;
	END PROCESS;
END Structure;