LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY Tarea6 IS
	PORT( KEY : IN std_logic_vector(0 downto 0);
			SW : IN std_logic_vector(0 downto 0);
			HEX0 : OUT std_logic_vector(6 downto 0);
			HEX1 : OUT std_logic_vector(6 downto 0);
			HEX2 : OUT std_logic_vector(6 downto 0);
			HEX3 : OUT std_logic_vector(6 downto 0);
			LEDR : OUT std_logic_vector(2 downto 0));
END Tarea6;

ARCHITECTURE Structure OF Tarea6 IS
COMPONENT driver7Segmentos IS
	PORT( codigoCaracter : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
			bitsCaracter : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END COMPONENT;

SIGNAL cont : integer := 0;
BEGIN
	Visor0 : driver7Segmentos
		Port Map( codigoCaracter => "011" + conv_std_logic_vector(cont, LEDR'length),
					 bitsCaracter => HEX0);
	Visor1 : driver7Segmentos
		Port Map( codigoCaracter => "010" + conv_std_logic_vector(cont, LEDR'length),
					 bitsCaracter => HEX1);
	Visor2 : driver7Segmentos
		Port Map( codigoCaracter => "001" + conv_std_logic_vector(cont, LEDR'length),
					 bitsCaracter => HEX2);
	Visor3 : driver7Segmentos
		Port Map( codigoCaracter => "000" + conv_std_logic_vector(cont, LEDR'length),
					 bitsCaracter => HEX3);
				 
	LEDR <= conv_std_logic_vector(cont, LEDR'length);
				 
	process (KEY(0))
	begin
		if rising_edge(KEY(0)) then
			if (SW(0) = '0') then 
				cont <= cont + 1;
			else
				cont <= cont - 1;
			end if;
		end if;
	end process;		
			
END Structure;