LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY GenClock IS
	GENERIC( Halves : integer := 1);
	PORT( ClockIN : IN std_logic;
			ClockOUT : OUT std_logic);
END GenClock;

ARCHITECTURE Structure OF GenClock IS
	SIGNAL cont : integer := 50000000/4 * Halves;
	SIGNAL clk : std_logic := '0';
BEGIN
	ClockOUT <= clk;

	PROCESS (ClockIN)
	BEGIN
		if rising_edge(ClockIN) then
			if (cont = 0) then
				clk <= not clk;
				cont <= 50000000/4 * Halves;
			else
				cont <= cont - 1;
			end if;
		end if;
	END PROCESS;
END Structure;