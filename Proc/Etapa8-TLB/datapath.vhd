LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.alucodes.all;

ENTITY datapath IS
  PORT (clk       : IN  STD_LOGIC;
        op        : IN  aluop;
        wrd       : IN  STD_LOGIC;
        addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed     : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        immed_x2  : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        ins_dad   : IN  STD_LOGIC;
        pc        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        in_d      : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
        rb_n	  : IN  STD_LOGIC;
        addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        aluout    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        z         : OUT STD_LOGIC;
        pc_act    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        rd_io	  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_io	  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        reset     : IN  STD_LOGIC;
        quirk     : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
        a_sys     : IN  STD_LOGIC;
        d_sys     : IN  STD_LOGIC;
		    int_en	  : OUT STD_LOGIC;
		    enter_sys : IN  STD_LOGIC;
        div_z     : OUT STD_LOGIC;
        eid       : IN STD_LOGIC_VECTOR(3 downto 0);
		privilege : OUT STD_LOGIC;
		wr_ins	  : IN STD_LOGIC;
		wr_dat	  : IN STD_LOGIC;
		wr_virt	  : IN STD_LOGIC;
		wr_phys	  : IN STD_LOGIC;
		miss_ins  : OUT STD_LOGIC;
		miss_dat  : OUT STD_LOGIC;
		v_ins_out : OUT STD_LOGIC;
		v_dat_out : OUT STD_LOGIC;
		prot_ins  : OUT STD_LOGIC;
		prot_dat  : OUT STD_LOGIC;
		r_dat_out : OUT STD_LOGIC);
END datapath;


ARCHITECTURE Structure OF datapath IS
  
  COMPONENT regfile IS
    PORT (clk       : IN  STD_LOGIC;
          wrd       : IN  STD_LOGIC;
          d         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          a         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          b         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          d_sys     : IN  STD_LOGIC;
          a_sys     : IN  STD_LOGIC;
          reset     : IN  STD_LOGIC;
          quirk     : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
          enter_sys : IN STD_LOGIC;
		      int_en	: OUT STD_LOGIC;
          eid : IN STD_LOGIC_VECTOR(3 downto 0);
          e_addr : IN STD_LOGIC_VECTOR(15 downto 0);
		  privilege : OUT STD_LOGIC);
  END COMPONENT;
  
  COMPONENT alu IS
    PORT (x  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          y  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op : IN  aluop;
          w  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          z  : OUT STD_LOGIC;
          div_z : OUT STD_LOGIC);
  END COMPONENT;
  
	component tlb is
	  port (boot    : in  std_logic;
			clk     : in  std_logic;
			wre 	: in  std_logic; -- write enable
			wrv 	: in  std_logic; -- write virtual address
			wri 	: in std_logic_vector(3 downto 0); -- write index
			vpn_in  : in  std_logic_vector(3 downto 0);
			ppn_in  : in  std_logic_vector(3 downto 0);
			v_in    : in  std_logic;
			r_in    : in  std_logic;
			-- salidas
			ppn_out : out std_logic_vector(3 downto 0);
			v_out   : out std_logic;
			r_out   : out std_logic;
			prot    : out std_logic;
			miss    : out std_logic);
	end component;
  
  SIGNAL seld, immfmt, rega, regb, wtod, mux_y, Iaddr_m, addr_reg : std_logic_vector(15 downto 0);
  -- señales TLBs
  signal wre_ins, wre_dat : std_logic;
  signal vpn_in_ins, vpn_in_dat, ppn_in, ppn_ins_out, ppn_dat_out : std_logic_vector(3 downto 0);
  signal v_in : std_logic;
  signal r_in : std_logic;
  --signal prot_ins, prot_dat, miss_ins, miss_dat : std_logic;
  signal is_access : boolean;
  signal wri : std_logic_vector(3 downto 0);
  
BEGIN
  
  with in_d select seld <= wtod when "00",
                           pc_act when "01",
                           rd_io when "10",
                           datard_m when others;

  immfmt <= shl(immed, (15 downto 1 => '0') & immed_x2);
  
  with rb_n select mux_y <= regb when '0',
                            immfmt when others;
  
  with ins_dad select Iaddr_m <= pc when '0',
                                wtod when others;
 
  data_wr <= regb;

  aluout <= wtod;
  
  wr_io <= regb;

  v_in <= regb(5);
  r_in <= regb(4);
	
  vpn_in_ins <= Iaddr_m(15 downto 12);
  vpn_in_dat <= Iaddr_m(15 downto 12);
  ppn_in <= regb(3 downto 0);
  wri <= rega(3 downto 0);
  
  is_access <= op = OPST or op = OPSTB or op = OPLD or op = OPLDB;
  
  addr_m <= ppn_ins_out & Iaddr_m(11 downto 0) when ins_dad = '0' else
			ppn_dat_out & Iaddr_m(11 downto 0) when ins_dad = '1' and is_access else
			(others => 'X');
			
--  tlb_v <= v_ins_out when ACCESO_INS,
--		   v_dat_out when ACCESO_DAT;
--  tlb_r <= r_ins_out when ACCESO_INS,
--		   r_dat_out when ACCESO_DAT;
--  tlb_prot <= prot_ins when ACCESO_INS,
--			  prot_dat when ACCESO_DAT;
--  tlb_miss <= miss_ins when ACCESO_INS,
--			  miss_dat when ACCESO_DAT;
			  
  reg0 : regfile PORT MAP (clk => clk,
                           wrd => wrd,
                           d => seld,
                           addr_a => addr_a,
                           addr_b => addr_b,
                           addr_d => addr_d,
                           a => rega,
                           b => regb,
                           d_sys => d_sys,
                           a_sys => a_sys,
                           reset => reset,
                           quirk => quirk,
                           enter_sys => enter_sys,
						               int_en => int_en,
                           eid => eid,
                           e_addr => addr_reg,
						   privilege => privilege);
  
  alu0 : alu PORT MAP (x => rega,
                       y => mux_y,
                       op => op,
                       w => wtod,
                       z => z,
                       div_z => div_z);
					   
  tlb_ins0 : tlb port map (boot => reset,
							clk => clk,
							wre => wr_ins,
							wrv => wr_virt,
							wri => wri,
							vpn_in => vpn_in_ins,
							ppn_in => ppn_in,
							v_in => v_in,
							r_in => r_in,
							ppn_out => ppn_ins_out,
							v_out => v_ins_out,
							prot => prot_ins,
							miss => miss_ins);

  tlb_dat0 : tlb port map (boot => reset,
							clk => clk,
							wre => wr_dat,
							wrv => wr_virt,
							wri => wri,
							vpn_in => vpn_in_dat,
							ppn_in => ppn_in,
							v_in => v_in,
							r_in => r_in,
							ppn_out => ppn_dat_out,
							v_out => v_dat_out,
							r_out => r_dat_out,
							prot => prot_dat,
							miss => miss_dat);

  PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      addr_reg <= Iaddr_m;
    end if;
  END PROCESS;

END Structure;
