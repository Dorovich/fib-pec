LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.alucodes.all;

ENTITY datapath IS
  PORT (clk      : IN  STD_LOGIC;
        op       : IN  aluop;
        wrd      : IN  STD_LOGIC;
        addr_a   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        immed_x2 : IN  STD_LOGIC;
        datard_m : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        ins_dad  : IN  STD_LOGIC;
        pc       : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        in_d     : IN  STD_LOGIC;
        rb_n	 : IN  STD_LOGIC;
        addr_m   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        data_wr  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
END datapath;


ARCHITECTURE Structure OF datapath IS

  -- Aqui iria la declaracion de las entidades que vamos a usar
  -- Usaremos la palabra reservada COMPONENT ...
  -- Tambien crearemos los cables/buses (signals) necesarios para unir las entidades
  
  COMPONENT regfile IS
    PORT (clk    : IN  STD_LOGIC;
          wrd    : IN  STD_LOGIC;
          d      : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_a : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          a      : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          b      : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
  END COMPONENT;
  
  COMPONENT alu IS
    PORT (x  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          y  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op : IN  aluop;
          w  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          z  : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL seld : std_logic_vector(15 downto 0);
  SIGNAL immfmt : std_logic_vector(15 downto 0);
  SIGNAL rega : std_logic_vector(15 downto 0);
  SIGNAL regb : std_logic_vector(15 downto 0);
  SIGNAL wtod : std_logic_vector(15 downto 0);
  SIGNAL mux_y : std_logic_vector(15 downto 0);
  
BEGIN

  -- Aqui iria la declaracion del "mapeo" (PORT MAP) de los nombres de las entradas/salidas de los componentes
  -- En los esquemas de la documentacion a la instancia del banco de registros le hemos llamado reg0 y a la de la alu le hemos llamado alu0

  with in_d select seld <= wtod when '0',
                           datard_m when others;

  immfmt <= shl(immed, "000000000000000" & immed_x2);
  
  with rb_n select mux_y <= regb when '0',
                            immfmt when others;
  
  with ins_dad select addr_m <= pc when '0',
                                wtod when others;
  
  data_wr <= regb;
  
  reg0 : regfile PORT MAP (clk => clk,
                           wrd => wrd,
                           d => seld,
                           addr_a => addr_a,
                           addr_b => addr_b,
                           addr_d => addr_d,
                           a => rega,
                           b => regb);
  
  alu0 : alu PORT MAP (x => rega,
                       y => mux_y,
                       op => op,
                       w => wtod);
  
END Structure;
