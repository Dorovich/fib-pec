LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE work.alucodes.all;

ENTITY proc IS
  PORT (clk       : IN  STD_LOGIC;
        boot      : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_m      : OUT STD_LOGIC;
        word_byte : OUT STD_LOGIC);
END proc;

ARCHITECTURE Structure OF proc IS

  -- Aqui iria la declaracion de las entidades que vamos a usar
  -- Usaremos la palabra reservada COMPONENT ...
  -- Tambien crearemos los cables/buses (signals) necesarios para unir las entidades

  COMPONENT unidad_control IS
    PORT (boot      : IN  STD_LOGIC;
          clk       : IN  STD_LOGIC;
          datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op        : OUT aluop;
          wrd       : OUT STD_LOGIC;
          addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          pc        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          ins_dad   : OUT STD_LOGIC;
          in_d      : OUT STD_LOGIC;
          immed_x2  : OUT STD_LOGIC;
          wr_m      : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
          rb_n      : OUT STD_LOGIC);
  END COMPONENT;
  
  COMPONENT datapath IS
    PORT (clk      : IN  STD_LOGIC;
          op 	   : IN  aluop;
          wrd      : IN  STD_LOGIC;
          addr_a   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          immed_x2 : IN  STD_LOGIC;
          datard_m : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          ins_dad  : IN  STD_LOGIC;
          pc       : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          in_d     : IN  STD_LOGIC;
          rb_n	   : IN STD_LOGIC;
          addr_m   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          data_wr  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
  END COMPONENT;
  
  SIGNAL Iimmed_x2, Iin_d, Iins_dad, Iwrd, Irb_n : std_logic;
  SIGNAL Iop : aluop;
  SIGNAL Iaddr_a, Iaddr_b, Iaddr_d : std_logic_vector(2 downto 0);
  SIGNAL Iimmed, Ipc : std_logic_vector(15 downto 0);

BEGIN

  -- Aqui iria la declaracion del "mapeo" (PORT MAP) de los nombres de las entradas/salidas de los componentes
  -- En los esquemas de la documentacion a la instancia del DATAPATH le hemos llamado e0 y a la de la unidad de control le hemos llamado c0

  c0 : unidad_control PORT MAP(boot      => boot,
                               clk       => clk,
                               datard_m  => datard_m,
                               op        => Iop,
                               wrd       => Iwrd,
                               addr_a    => Iaddr_a,
                               addr_b    => Iaddr_b,
                               addr_d    => Iaddr_d,
                               immed     => Iimmed,
                               pc        => Ipc,
                               ins_dad   => Iins_dad,
                               in_d      => Iin_d,
                               immed_x2  => Iimmed_x2,
                               wr_m      => wr_m,
                               word_byte => word_byte,
                               rb_n 	 => Irb_n);
  
  e0 : datapath PORT MAP(clk      => clk,
                         op       => Iop,
                         wrd      => Iwrd,
                         addr_a   => Iaddr_a,
                         addr_b   => Iaddr_b,
                         addr_d   => Iaddr_d,
                         immed    => Iimmed,
                         immed_x2 => Iimmed_x2,
                         datard_m => datard_m,
                         ins_dad  => Iins_dad,
                         pc       => Ipc,
                         in_d     => Iin_d,
                         rb_n     => Irb_n,
                         addr_m   => addr_m,
                         data_wr  => data_wr);
  
END Structure;
