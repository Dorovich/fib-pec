LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY driver7Segmentos IS
	PORT( codigoCaracter : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			bitsCaracter : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END driver7Segmentos;

ARCHITECTURE Structure OF driver7Segmentos IS
BEGIN
	with codigoCaracter select bitsCaracter <= "1000000" when x"0",
															 "1111001" when x"1",
															 "0100100" when x"2",
															 "0110000" when x"3",
															 "0011001" when x"4",
															 "0010010" when x"5",
															 "0000010" when x"6",
															 "1111000" when x"7",
															 "0000000" when x"8",
															 "0011000" when x"9",
															 "0001000" when x"A",
															 "0000011" when x"B",
															 "1000110" when x"C",
															 "0100001" when x"D",
															 "0000110" when x"E",
															 "0001110" when others;
END Structure;