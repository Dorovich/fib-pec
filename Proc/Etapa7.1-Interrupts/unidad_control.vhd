LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.alucodes.all;

ENTITY unidad_control IS
  PORT (boot      : IN  STD_LOGIC;
        clk       : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        op        : OUT aluop;
        wrd       : OUT STD_LOGIC;
        addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        pc        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        ins_dad   : OUT STD_LOGIC;
        in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        immed_x2  : OUT STD_LOGIC;
        wr_m      : OUT STD_LOGIC;
        rb_n	  : OUT STD_LOGIC;
        word_byte : OUT STD_LOGIC;
        aluout    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        z         : IN STD_LOGIC;
        pc_act    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        addr_io   : OUT STD_LOGIC_VECTOR(7 downto 0);
        rd_in	  : OUT STD_LOGIC;
        wr_out	  : OUT STD_LOGIC;
        quirk     : OUT STD_LOGIC_VECTOR(1 downto 0);
        a_sys     : OUT STD_LOGIC;
        d_sys     : OUT STD_LOGIC;
        int_en	  : IN  STD_LOGIC;
        int_rq	  : IN  STD_LOGIC;
        enter_sys : OUT STD_LOGIC;
        inta      : OUT STD_LOGIC);
END unidad_control;


ARCHITECTURE Structure OF unidad_control IS

  -- Aqui iria la declaracion de las entidades que vamos a usar
  -- Tambien crearemos los cables/buses (signals) necesarios para unir las entidades
  -- Aqui iria la definicion del program counter y del registro IR

  COMPONENT control_l IS
    PORT (ir        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op        : OUT aluop;
          ldpc      : OUT STD_LOGIC;
          wrd       : OUT STD_LOGIC;
          addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          wr_m      : OUT STD_LOGIC;
          in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
          immed_x2  : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
          rb_n	    : OUT STD_LOGIC;
          addr_io   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
          rd_in	    : OUT STD_LOGIC;
          wr_out    : OUT STD_LOGIC;
          quirk     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
          a_sys     : OUT STD_LOGIC;
          d_sys     : OUT STD_LOGIC;
          enter_sys : IN  STD_LOGIC;
          inta      : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT multi IS
    PORT(clk       : IN  STD_LOGIC;
         boot      : IN  STD_LOGIC;
         ldpc_l    : IN  STD_LOGIC;
         wrd_l     : IN  STD_LOGIC;
         wr_m_l    : IN  STD_LOGIC;
         w_b       : IN  STD_LOGIC;
         ldpc      : OUT STD_LOGIC;
         wrd       : OUT STD_LOGIC;
         wr_m      : OUT STD_LOGIC;
         ldir      : OUT STD_LOGIC;
         ins_dad   : OUT STD_LOGIC;
         word_byte : OUT STD_LOGIC;
         int_en	   : IN  STD_LOGIC;
         int_rq	   : IN  STD_LOGIC;
         enter_sys : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL next_pc_tmp, next_pc, pc_reg, pc_interrupt : std_logic_vector(15 downto 0) := x"C000";
  SIGNAL br_forwards, br_backwards, br_selected : std_logic_vector(15 downto 0);
  SIGNAL ir_reg : std_logic_vector(15 downto 0);
  SIGNAL act_pc : std_logic;
  SIGNAL act_ir : std_logic;
  SIGNAL Iop : aluop;
  SIGNAL Iwr_m : std_logic;
  SIGNAL Ildpc : std_logic;
  SIGNAL Iwrd : std_logic;
  SIGNAL Iwb : std_logic;
  SIGNAL tknbr : std_logic_vector(1 downto 0);
  SIGNAL Ienter_sys : std_logic;
  SIGNAL Iinta : std_logic;
  SIGNAL jz_jump, jnz_jump, bz_jump, bnz_jump, absolute_jump : boolean;
BEGIN

  c0 : control_l PORT MAP(ir => ir_reg,
                          op => Iop,
                          ldpc => Ildpc,
                          wrd => Iwrd,
                          addr_a => addr_a,
                          addr_b => addr_b,
                          addr_d => addr_d,
                          immed => immed,
                          wr_m => Iwr_m,
                          in_d => in_d,
                          immed_x2 => immed_x2,
                          rb_n => rb_n,
                          word_byte => Iwb,
                          addr_io => addr_io,
                          rd_in => rd_in,
                          wr_out => wr_out,
                          quirk => quirk,
                          a_sys => a_sys,
                          d_sys => d_sys,
                          enter_sys => Ienter_sys,
                          inta => inta);
  
  m0 : multi PORT MAP(clk => clk,
                      boot => boot,
                      ldpc_l => Ildpc,
                      wrd_l => Iwrd,
                      wr_m_l => Iwr_m,
                      w_b => Iwb,
                      ldpc => act_pc,
                      wrd => wrd,
                      wr_m => wr_m,
                      ldir => act_ir,
                      ins_dad => ins_dad,
                      word_byte => word_byte,
                      int_en => int_en,
                      int_rq => int_rq,
                      enter_sys => Ienter_sys);

  enter_sys <= Ienter_sys;
  op <= Iop;

  jz_jump <= Iop = OPJZ and z = '1';
  jnz_jump <= Iop = OPJNZ and z = '0';
  bz_jump <= Iop = OPBZ and z = '1';
  bnz_jump <= Iop = OPBNZ and z = '0';
  absolute_jump <= Iop = OPJMP or Iop = OPJAL or Iop = OPRETI or jz_jump or jnz_jump;
  
  br_backwards <= pc_reg + 2 - std_logic_vector(abs(signed(shl(ir_reg(7 downto 0), x"01"))));
  br_forwards <= pc_reg + 2 + std_logic_vector(unsigned(shl(ir_reg(7 downto 0), x"01")));
  
  br_selected <= br_backwards when signed(ir_reg(7 downto 0)) < 0 else
				 br_forwards;

  tknbr <= "01" when bz_jump or bnz_jump else
           "10" when Ienter_sys = '1' or absolute_jump else
           "00";
  
  with tknbr select next_pc_tmp <= pc_reg + 2 when "00",
                                  br_selected when "01",
                                  aluout when "10",
                                  pc_reg when others;

  with boot select next_pc <= next_pc_tmp when '0',
                              x"C000" when '1',
                              (others => 'X') when others;

  pc <= pc_reg;
  pc_act <= pc_reg + 2 when Iop = OPJAL and Ienter_sys = '0' else
			pc_interrupt when Ienter_sys = '1' else
			next_pc;

  PROCESS (boot,clk)
  BEGIN
    if rising_edge(clk) then
      if boot = '1' then
        pc_reg <= x"C000";
      elsif act_pc = '1' then
        pc_reg <= next_pc;
      end if;
	  pc_interrupt <= next_pc;
    end if;
  END PROCESS;

  PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      if act_ir = '1' then
        ir_reg <= datard_m;
      end if;
    end if;
  END PROCESS;
  
END Structure;
