LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE work.alucodes.all;

ENTITY control_l IS
  PORT (ir        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        op 	  : OUT aluop;
        ldpc      : OUT STD_LOGIC;
        wrd       : OUT STD_LOGIC;
        addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_m      : OUT STD_LOGIC;
        in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        immed_x2  : OUT STD_LOGIC;
        word_byte : OUT STD_LOGIC;
        rb_n	  : OUT STD_LOGIC;
        addr_io   : OUT STD_LOGIC_VECTOR(7 downto 0);
        rd_in	  : OUT STD_LOGIC;
        wr_out	  : OUT STD_LOGIC;
        quirk	  : OUT STD_LOGIC_VECTOR(1 downto 0);
        a_sys     : OUT STD_LOGIC;
        d_sys     : OUT STD_LOGIC;
	enter_sys : IN  STD_LOGIC;
        inta      : OUT STD_LOGIC);
END control_l;


ARCHITECTURE Structure OF control_l IS
  SIGNAL opcode : std_logic_vector(3 downto 0);
  SIGNAL f : std_logic_vector(2 downto 0);
  SIGNAL f2 : std_logic_vector(5 downto 0);
  SIGNAL b : std_logic;
  SIGNAL op_tmp : aluop;
BEGIN

  -- Aqui iria la generacion de las senales de control del datapath
  
  opcode <= ir(15 downto 12);
  f <= ir(5 downto 3);
  f2 <= ir(5 downto 0);
  b <= ir(8);
  op <=  op_tmp when enter_sys = '0' else OPJMP;  -- para pasar @RSG
  op_tmp <= OPAND 	 when opcode = x"0" and f = "000" else -- ops logicas y aritmeticas
        OPOR	 when opcode = x"0" and f = "001" else
        OPXOR 	 when opcode = x"0" and f = "010" else
        OPNOT 	 when opcode = x"0" and f = "011" else
        OPADD 	 when opcode = x"0" and f = "100" else
        OPSUB 	 when opcode = x"0" and f = "101" else
        OPSHA 	 when opcode = x"0" and f = "110" else
        OPSHL 	 when opcode = x"0" and f = "111" else
        OPCMPLT  when opcode = x"1" and f = "000" else -- comparacion con/sin signo
        OPCMPLE  when opcode = x"1" and f = "001" else
        OPCMPEQ  when opcode = x"1" and f = "011" else
        OPCMPLTU when opcode = x"1" and f = "100" else
        OPCMPLEU when opcode = x"1" and f = "101" else
        OPADDI   when opcode = x"2"               else -- add inmediato
        OPLD     when opcode = x"3"               else -- load
        OPST 	 when opcode = x"4"               else -- store
        OPMOVI   when opcode = x"5" and b = '0'   else -- mover inmediato
        OPMOVHI  when opcode = x"5" and b = '1'   else
        OPBZ     when opcode = x"6" and b = '0'   else -- branch
        OPBNZ    when opcode = x"6" and b = '1'   else
        OPIN	 when opcode = x"7" and b = '0'   else -- i/o
        OPOUT	 when opcode = x"7" and b = '1'   else
        OPMUL    when opcode = x"8" and f = "000" else -- extension artimetica
        OPMULH   when opcode = x"8" and f = "001" else
        OPMULHU  when opcode = x"8" and f = "010" else
        OPDIV    when opcode = x"8" and f = "100" else
        OPDIVU   when opcode = x"8" and f = "101" else
        OPJZ     when opcode = x"A" and f2 = "000000" else -- jumps
        OPJNZ    when opcode = x"A" and f2 = "000001" else
        OPJMP    when opcode = x"A" and f2 = "000011" else
        OPJAL    when opcode = x"A" and f2 = "000100" else
        OPLDB 	 when opcode = x"D"               else -- load byte
        OPSTB 	 when opcode = x"E"               else -- store byte
        OPEI	 when opcode = x"F" and f2 = "100000" else -- enable interrupts
        OPDI	 when opcode = x"F" and f2 = "100001" else -- disable interrupts
        OPRETI	 when opcode = x"F" and f2 = "100100" else -- return from interrupt
        OPGETIID when opcode = x"F" and f2 = "101000" else -- get interrupt id
        OPRDS	 when opcode = x"F" and f2 = "101100" else -- read system register
        OPWRS	 when opcode = x"F" and f2 = "110000" else -- write system register
        OPHALT; -- halt
  
  inta <= '1' when opcode = x"F" and f2 = "101000" else '0';

  ldpc <= '0' when ir(15 downto 0) = x"FFFF" else '1';
  
  wrd <= '1' when opcode = x"0" or
         opcode = x"1" or
         opcode = x"2" or
         opcode = x"3" or
         opcode = x"5" or
         (opcode = x"7" and b = '0') or -- in
         opcode = x"8" or
         (opcode = x"A" and f2 = "000100") or -- jal
         opcode = x"D" or
         (opcode = x"F" and f2 = "101100") or -- rds
         (opcode = x"F" and f2 = "110000") or -- wrs
		 (opcode = x"F" and f2 = "101000") or -- getiid
		 enter_sys = '1' -- interrupting
         else '0';
  
  addr_d <= ir(11 downto 9);
  
  rd_in <= '1' when (opcode = x"7" and b = '0') or (opcode = x"F" and f2 = "101000") else '0';
  wr_out <= '1' when opcode = x"7" and b = '1' else '0';
  addr_io <= ir(7 downto 0) when opcode = x"7" else (7 downto 0 => 'X');
  
  quirk <= "00" when opcode = x"F" and f2 = "100000" else -- EI
           "01" when opcode = x"F" and f2 = "100001" else -- DI
           "10" when opcode = x"F" and f2 = "100100" else -- RETI
           "11"; -- para el futuro

  with opcode select addr_a <= ir(11 downto 9) when x"5",
                               ir(8 downto 6) when others;

  with opcode select addr_b <= ir(2 downto 0) when x"0",
                               ir(2 downto 0) when x"1",
                               ir(2 downto 0) when x"8",
                               ir(11 downto 9) when others;
  
  with opcode select immed <= sxt(ir(7 downto 0), immed'length) when x"5",
                              sxt(ir(7 downto 0), immed'length) when x"6",
                              sxt(ir(7 downto 0), immed'length) when x"7",
                              sxt(ir(5 downto 0), immed'length) when others;
  
  in_d <= "01" when (opcode = x"A" and f2 = "000100") or enter_sys = '1' else -- jal/interrupting
          "10" when (opcode = x"7" and b = '0') or (opcode = x"F" and f2 = "101000") else -- in/getiid
          "11" when opcode = x"3" or opcode = x"D" else -- load, loadb
          "00";
  
  wr_m <= '1' when opcode = x"4" or
          opcode = x"E"
          else '0';
  
  immed_x2 <= '1' when opcode = x"3" or
              opcode = x"4" or
              opcode = x"6"
              else '0';
  
  word_byte <= '1' when opcode = x"D" or
               opcode = x"E"
               else '0';
  
  rb_n <= '1' when opcode = x"2" or
          opcode = x"3" or
          opcode = x"4" or
          opcode = x"5" or
          opcode = x"7" or
          opcode = x"D" or 
          opcode = x"E"
          else '0';

  a_sys <= '1' when opcode = x"F" and f2 = "101100" -- rds
           else '0';

  d_sys <= '1' when (opcode = x"F" and f2 = "110000") or -- wrs
           (opcode = x"F" and f2 = "100100") -- reti
           else '0';
  
END Structure;
