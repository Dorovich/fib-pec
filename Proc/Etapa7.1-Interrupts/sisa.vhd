LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY sisa IS
  PORT (CLOCK_50  : IN    STD_LOGIC;
        SRAM_ADDR : out   std_logic_vector(17 downto 0);
        SRAM_DQ   : inout std_logic_vector(15 downto 0);
        SRAM_UB_N : out   std_logic;
        SRAM_LB_N : out   std_logic;
        SRAM_CE_N : out   std_logic := '1';
        SRAM_OE_N : out   std_logic := '1';
        SRAM_WE_N : out   std_logic := '1';
        LEDG 	  : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        LEDR 	  : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        HEX0	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
        HEX1	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
        HEX2	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
        HEX3	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
        SW 		  : IN    STD_LOGIC_VECTOR(9 DOWNTO 0);
        KEY 	  : IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
        PS2_CLK	  : INOUT STD_LOGIC;
        PS2_DAT	  : INOUT STD_LOGIC;
        VGA_R	  : OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
        VGA_G	  : OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
        VGA_B	  : OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
        VGA_HS	  : OUT   STD_LOGIC;
        VGA_VS	  : OUT   STD_LOGIC);
END sisa;

ARCHITECTURE Structure OF sisa IS

  COMPONENT proc IS
    PORT (clk       : IN  STD_LOGIC;
          boot      : IN  STD_LOGIC;
          datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          wr_m      : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
          addr_io 	: OUT STD_LOGIC_VECTOR(7 downto 0);
          rd_in		: OUT STD_LOGIC;
          rd_io		: IN  STD_LOGIC_VECTOR(15 downto 0);
          wr_out	: OUT STD_LOGIC;
          wr_io		: OUT STD_LOGIC_VECTOR(15 downto 0);
		  intr      : in  std_logic;
		  inta 		: out std_logic);
  END COMPONENT;

  COMPONENT MemoryController is
    port (CLOCK_50  : in  std_logic;
          addr      : in  std_logic_vector(15 downto 0);
          wr_data   : in  std_logic_vector(15 downto 0);
          rd_data   : out std_logic_vector(15 downto 0);
          we        : in  std_logic;
          byte_m    : in  std_logic;
          vga_addr    : out std_logic_vector(12 downto 0);
          vga_we      : out std_logic;
          vga_wr_data : out std_logic_vector(15 downto 0);
          vga_rd_data : in  std_logic_vector(15 downto 0);
          vga_byte_m  : out std_logic;
          SRAM_ADDR : out   std_logic_vector(17 downto 0);
          SRAM_DQ   : inout std_logic_vector(15 downto 0);
          SRAM_UB_N : out   std_logic;
          SRAM_LB_N : out   std_logic;
          SRAM_CE_N : out   std_logic := '1';
          SRAM_OE_N : out   std_logic := '1';
          SRAM_WE_N : out   std_logic := '1');
  end COMPONENT;
  
  COMPONENT controladores_IO IS
    PORT (boot : IN STD_LOGIC;
          CLOCK_50 : IN std_logic;
          addr_io : IN std_logic_vector(7 downto 0);
          wr_io : in std_logic_vector(15 downto 0);
          rd_io : out std_logic_vector(15 downto 0);
          wr_out : in std_logic;
          rd_in : in std_logic;
          led_verdes : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
          led_rojos : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
          visor0 : OUT STD_LOGIC_VECTOR(6 downto 0);
          visor1 : OUT STD_LOGIC_VECTOR(6 downto 0);
          visor2 : OUT STD_LOGIC_VECTOR(6 downto 0);
          visor3 : OUT STD_LOGIC_VECTOR(6 downto 0);
          switches : IN STD_LOGIC_VECTOR(7 downto 0);
          keys : IN STD_LOGIC_VECTOR(3 downto 0);
          ps2_clk : inout std_logic;
          ps2_data : inout std_logic;
          vga_cursor : out std_logic_vector(15 downto 0);
          vga_cursor_enable : out std_logic;
		  intr : out std_logic;
		  inta : in std_logic);
  END COMPONENT;
  
  COMPONENT vga_controller is
    PORT(clk_50mhz      : in  std_logic;
         reset          : in  std_logic;
         blank_out      : out std_logic;
         csync_out      : out std_logic;
         red_out        : out std_logic_vector(7 downto 0);
         green_out      : out std_logic_vector(7 downto 0);
         blue_out       : out std_logic_vector(7 downto 0);
         horiz_sync_out : out std_logic;
         vert_sync_out  : out std_logic;
         --
         addr_vga          : in std_logic_vector(12 downto 0);
         we                : in std_logic;
         wr_data           : in std_logic_vector(15 downto 0);
         rd_data           : out std_logic_vector(15 downto 0);
         byte_m            : in std_logic;
         vga_cursor        : in std_logic_vector(15 downto 0);
         vga_cursor_enable : in std_logic);
  end COMPONENT;
  
  SIGNAL clk_fmt : std_logic_vector (2 downto 0) := "000";
  SIGNAL wr_m_out, word_byte_out : std_logic;
  SIGNAL rd_data_out, data_wr_out, addr_m_out : std_logic_vector(15 downto 0);
  SIGNAL rd_io_in, wr_io_out  : std_logic;
  SIGNAL data_io_addr : std_logic_vector(7 downto 0);
  SIGNAL data_io_in, data_io_out : std_logic_vector(15 downto 0);
  -- vga
  SIGNAL mem_addr_vga : std_logic_vector(12 downto 0);
  SIGNAL we_vga, byte_m_vga, cursor_enable_vga : std_logic;
  SIGNAL wr_data_vga, rd_data_vga, cursor_vga : std_logic_vector(15 downto 0);
  SIGNAL red_vga, green_vga, blue_vga : std_logic_vector(7 downto 0);
  SIGNAL Iintr, Iinta : std_logic;
  
BEGIN

  pro0 : proc PORT MAP (clk => clk_fmt(2),
                        boot => SW(9),
                        datard_m => rd_data_out,
                        addr_m => addr_m_out,
                        data_wr => data_wr_out,
                        wr_m => wr_m_out,
                        word_byte => word_byte_out,
                        addr_io => data_io_addr,
                        rd_in => rd_io_in,
                        rd_io => data_io_in,
                        wr_out => wr_io_out,
                        wr_io => data_io_out,
						intr => Iintr,
						inta => Iinta);
  
  mem0: MemoryController PORT MAP (CLOCK_50 => CLOCK_50,
                                   addr => addr_m_out,
                                   wr_data => data_wr_out,
                                   rd_data => rd_data_out,
                                   we => wr_m_out,
                                   byte_m => word_byte_out,
                                   vga_addr => mem_addr_vga,
                                   vga_we => we_vga,
                                   vga_wr_data => wr_data_vga,
                                   vga_rd_data => rd_data_vga,
                                   vga_byte_m => byte_m_vga,
                                   SRAM_ADDR => SRAM_ADDR,
                                   SRAM_DQ => SRAM_DQ,
                                   SRAM_UB_N => SRAM_UB_N,
                                   SRAM_LB_N => SRAM_LB_N,
                                   SRAM_CE_N => SRAM_CE_N,
                                   SRAM_OE_N => SRAM_OE_N,
                                   SRAM_WE_N => SRAM_WE_N);
  
  io0: controladores_IO PORT MAP (boot => SW(9),
                                  CLOCK_50 => CLOCK_50,
                                  addr_io => data_io_addr,
                                  wr_io => data_io_out,
                                  rd_io => data_io_in,
                                  wr_out => wr_io_out,
                                  rd_in => rd_io_in,
                                  led_verdes => LEDG,
                                  led_rojos => LEDR,
                                  visor0 => HEX0,
                                  visor1 => HEX1,
                                  visor2 => HEX2,
                                  visor3 => HEX3,
                                  switches => SW(7 downto 0),
                                  keys => KEY,
                                  ps2_clk => PS2_CLK,
                                  ps2_data => PS2_DAT,
                                  vga_cursor => cursor_vga,
                                  vga_cursor_enable => cursor_enable_vga,
								  intr => Iintr,
								  inta => Iinta);
  
  
  vid0 : vga_controller PORT MAP (clk_50mhz => CLOCK_50,
                                  reset => SW(9),
                                  horiz_sync_out => VGA_HS,
                                  vert_sync_out => VGA_VS,
                                  red_out => red_vga,
                                  green_out => green_vga,
                                  blue_out => blue_vga,
                                  addr_vga => mem_addr_vga,
                                  we => we_vga,
                                  wr_data => wr_data_vga,
                                  rd_data => rd_data_vga,
                                  byte_m => byte_m_vga,
                                  vga_cursor => cursor_vga,
                                  vga_cursor_enable => cursor_enable_vga);
  
  VGA_R <= red_vga(3 downto 0);
  VGA_G <= green_vga(3 downto 0);
  VGA_B <= blue_vga(3 downto 0);

  PROCESS (CLOCK_50)
  BEGIN
    if rising_edge(CLOCK_50) then
      clk_fmt <= clk_fmt + '1';
    end if;
  END PROCESS;
  
END Structure;
