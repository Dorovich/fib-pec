library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity SRAMController is
    port (clk         : in    std_logic;
          -- señales para la placa de desarrollo
          SRAM_ADDR   : out   std_logic_vector(17 downto 0);
          SRAM_DQ     : inout std_logic_vector(15 downto 0);
          SRAM_UB_N   : out   std_logic;
          SRAM_LB_N   : out   std_logic;
          SRAM_CE_N   : out   std_logic := '1';
          SRAM_OE_N   : out   std_logic := '1';
          SRAM_WE_N   : out   std_logic := '1';
          -- señales internas del procesador
          address     : in    std_logic_vector(15 downto 0) := x"0000";
          dataRead    : out   std_logic_vector(15 downto 0);
          dataToWrite : in    std_logic_vector(15 downto 0);
          WR          : in    std_logic;
          byte_m      : in    std_logic := '0');
end SRAMController;

architecture comportament of SRAMController is
  type estado_t is (RD, WR_INI, WR_FIN);
  signal estado : estado_t := RD;
  signal byte : std_logic_vector(7 downto 0);
  signal word : std_logic_vector(15 downto 0);
  signal cont : integer := 0;
begin

  SRAM_CE_N <= '0';
  SRAM_OE_N <= '0';
  SRAM_ADDR <= "000" & address(15 downto 1);
  
  byte <= SRAM_DQ(7 downto 0) when address(0) = '0' else SRAM_DQ(15 downto 8);
  word <= x"00" & byte when byte(7) = '0' else x"FF" & byte;
  
  
  
  -- logica proximo estado
  process (clk)
  begin
    if rising_edge(clk) then
      case estado is
        when RD =>
          if WR = '1' then
            estado <= WR_INI;
          end if;
          
        when WR_INI =>
          estado <= WR_FIN;
          
        when WR_FIN =>
			 cont <= cont + 1;
			 if cont = 7 then
				estado <= RD;
				cont <= 0;
			 end if;
      end case;
    end if;
  end process;

  -- logica salidas
  process (clk,estado)
  begin
    case estado is
      when RD =>
        SRAM_WE_N <= '1';
        SRAM_UB_N <= '0';
        SRAM_LB_N <= '0';
		  if byte_m = '0' then
				dataRead <= SRAM_DQ;
		  else
				dataRead <= word;
		  end if;
        SRAM_DQ <= (others => 'Z');
        
      when WR_INI =>
        SRAM_WE_N <= '0';
		  if byte_m = '1' then
				if address(0) = '0' then
					SRAM_UB_N <= '1';
					SRAM_LB_N <= '0';
					SRAM_DQ <= "ZZZZZZZZ" & dataToWrite(7 downto 0);
				else
					SRAM_UB_N <= '0';
					SRAM_LB_N <= '1';
					SRAM_DQ <= dataToWrite(7 downto 0) & "ZZZZZZZZ";
				end if;
		  else
			   SRAM_UB_N <= '0';
			   SRAM_LB_N <= '0';
				SRAM_DQ <= dataToWrite;
		  end if;
        
      when WR_FIN =>
        SRAM_WE_N <= '1';
    end case;
  end process;
end comportament;
