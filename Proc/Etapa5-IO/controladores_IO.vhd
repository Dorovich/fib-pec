LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

ENTITY controladores_IO IS
	PORT (boot : IN STD_LOGIC;
		  CLOCK_50 : IN std_logic;
		  addr_io : IN std_logic_vector(7 downto 0);
		  wr_io : in std_logic_vector(15 downto 0);
		  rd_io : out std_logic_vector(15 downto 0);
		  wr_out : in std_logic;
		  rd_in : in std_logic;
		  led_verdes : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		  led_rojos : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		  visor0 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor1 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor2 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor3 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  interruptores : IN STD_LOGIC_VECTOR(7 downto 0);
		  pulsadores : IN STD_LOGIC_VECTOR(3 downto 0));
END controladores_IO;

ARCHITECTURE Structure OF controladores_IO IS

	COMPONENT driver7segmentos IS
		PORT( codigoCaracter : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
				bitsCaracter : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;


	type REG_ARR is array (255 downto 0) of std_logic_vector(15 downto 0);
	signal ioreg : REG_ARR;
	signal v0_out, v1_out, v2_out, v3_out : std_LOGIC_VECTOR(6 downto 0);
BEGIN

	v0: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(3 downto 0),
											 bitsCaracter => v0_out);
											 
	v1: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(7 downto 4),
											 bitsCaracter => v1_out);
											 
	v2: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(11 downto 8),
											 bitsCaracter => v2_out);
											 
	v3: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(15 downto 12),
											 bitsCaracter => v3_out);
	
	led_verdes <= ioreg(5)(7 downto 0);
	led_rojos <= ioreg(6)(7 downto 0);
	visor0 <= v0_out when ioreg(9)(0) = '1' else (others => '1');
	visor1 <= v1_out when ioreg(9)(1) = '1' else (others => '1');
	visor2 <= v2_out when ioreg(9)(2) = '1' else (others => '1');
	visor3 <= v3_out when ioreg(9)(3) = '1' else (others => '1');
	
	rd_io <= ioreg(to_integer(unsigned(addr_io))) when rd_in = '1' else (others => 'X');

	process (CLOCK_50)
	begin
		if rising_edge(CLOCK_50) then
			ioreg(7)(3 downto 0) <= pulsadores;
			ioreg(8)(7 downto 0) <= interruptores;
			if wr_out = '1' then
				if (not unsigned(addr_io) = 7 and not unsigned(addr_io) = 8) then
					ioreg(to_integer(unsigned(addr_io))) <= wr_io;
				end if;
			end if;
		end if;
	end process;
END Structure;