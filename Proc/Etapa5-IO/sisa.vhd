LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY sisa IS
  PORT (CLOCK_50  : IN    STD_LOGIC;
        SRAM_ADDR : out   std_logic_vector(17 downto 0);
        SRAM_DQ   : inout std_logic_vector(15 downto 0);
        SRAM_UB_N : out   std_logic;
        SRAM_LB_N : out   std_logic;
        SRAM_CE_N : out   std_logic := '1';
        SRAM_OE_N : out   std_logic := '1';
        SRAM_WE_N : out   std_logic := '1';
		LEDG 	  : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
		LEDR 	  : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX0	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
		HEX1	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
		HEX2	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
		HEX3	  : OUT   STD_LOGIC_VECTOR(6 downto 0);
		SW 		  : IN    STD_LOGIC_VECTOR(9 DOWNTO 0);
		KEY 	  : IN    STD_LOGIC_VECTOR(3 DOWNTO 0));
END sisa;

ARCHITECTURE Structure OF sisa IS

  COMPONENT proc IS
    PORT (clk       : IN  STD_LOGIC;
          boot      : IN  STD_LOGIC;
          datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          wr_m      : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
		  addr_io 	: OUT STD_LOGIC_VECTOR(7 downto 0);
		  rd_in		: OUT STD_LOGIC;
		  rd_io		: IN  STD_LOGIC_VECTOR(15 downto 0);
		  wr_out	: OUT STD_LOGIC;
		  wr_io		: OUT STD_LOGIC_VECTOR(15 downto 0));
  END COMPONENT;

  COMPONENT MemoryController is
    port (CLOCK_50  : in  std_logic;
          addr      : in  std_logic_vector(15 downto 0);
          wr_data   : in  std_logic_vector(15 downto 0);
          rd_data   : out std_logic_vector(15 downto 0);
          we        : in  std_logic;
          byte_m    : in  std_logic;
          SRAM_ADDR : out   std_logic_vector(17 downto 0);
          SRAM_DQ   : inout std_logic_vector(15 downto 0);
          SRAM_UB_N : out   std_logic;
          SRAM_LB_N : out   std_logic;
          SRAM_CE_N : out   std_logic := '1';
          SRAM_OE_N : out   std_logic := '1';
          SRAM_WE_N : out   std_logic := '1');
  end COMPONENT;
  
  COMPONENT controladores_IO IS
	PORT (boot : IN STD_LOGIC;
		  CLOCK_50 : IN std_logic;
		  addr_io : IN std_logic_vector(7 downto 0);
		  wr_io : in std_logic_vector(15 downto 0);
		  rd_io : out std_logic_vector(15 downto 0);
		  wr_out : in std_logic;
		  rd_in : in std_logic;
		  led_verdes : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		  led_rojos : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		  visor0 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor1 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor2 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  visor3 : OUT STD_LOGIC_VECTOR(6 downto 0);
		  interruptores : IN STD_LOGIC_VECTOR(7 downto 0);
		  pulsadores : IN STD_LOGIC_VECTOR(3 downto 0));
  END COMPONENT;
  
  SIGNAL clk_fmt : std_logic_vector (2 downto 0) := "000";
  SIGNAL wr_m_out, word_byte_out : std_logic;
  SIGNAL rd_data_out, data_wr_out, addr_m_out : std_logic_vector(15 downto 0);
  SIGNAL rd_io_in, wr_io_out  : std_logic;
  SIGNAL data_io_addr : std_logic_vector(7 downto 0);
  SIGNAL data_io_in, data_io_out : std_logic_vector(15 downto 0);
 
BEGIN

  pro0 : proc PORT MAP (clk => clk_fmt(2),
                        boot => SW(9),
                        datard_m => rd_data_out,
                        addr_m => addr_m_out,
                        data_wr => data_wr_out,
                        wr_m => wr_m_out,
                        word_byte => word_byte_out,
						addr_io => data_io_addr,
						rd_in => rd_io_in,
						rd_io => data_io_in,
						wr_out => wr_io_out,
						wr_io => data_io_out);
  
  mem0: MemoryController PORT MAP (CLOCK_50 => CLOCK_50,
                                   addr => addr_m_out,
                                   wr_data => data_wr_out,
                                   rd_data => rd_data_out,
                                   we => wr_m_out,
                                   byte_m => word_byte_out,
                                   SRAM_ADDR => SRAM_ADDR,
                                   SRAM_DQ => SRAM_DQ,
                                   SRAM_UB_N => SRAM_UB_N,
                                   SRAM_LB_N => SRAM_LB_N,
                                   SRAM_CE_N => SRAM_CE_N,
                                   SRAM_OE_N => SRAM_OE_N,
                                   SRAM_WE_N => SRAM_WE_N);
								   
  io0: controladores_IO PORT MAP (boot => SW(9),
								  CLOCK_50 => CLOCK_50,
								  addr_io => data_io_addr,
								  wr_io => data_io_out,
								  rd_io => data_io_in,
								  wr_out => wr_io_out,
								  rd_in => rd_io_in,
								  led_verdes => LEDG,
								  led_rojos => LEDR,
								  visor0 => HEX0,
								  visor1 => HEX1,
								  visor2 => HEX2,
								  visor3 => HEX3,
								  interruptores => SW(7 downto 0),
								  pulsadores => KEY);

  PROCESS (CLOCK_50)
  BEGIN
    if rising_edge(CLOCK_50) then
      clk_fmt <= clk_fmt + '1';
    end if;
  END PROCESS;
  
END Structure;
