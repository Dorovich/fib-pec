	.macro $movei p1 imm16
        movi    \p1, lo(\imm16)
        movhi   \p1, hi(\imm16)
	.endm


	.text
	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	;; Inicializacion
	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	$MOVEI 	r1, RSG
	wrs    	s5, r1
	movi   	r1, 0xF
	out     9, r1
	movi   	r1, 0xFF
	out    	10, r1
	$MOVEI 	r6, inici
	jmp	r6

	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	;; Rutina de servicio de interrupcion
	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
RSG:   	rds	r7, s2
	out    	10, r7
	reti


	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	;; Rutina principal
	;; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
inici: 
	$movei 	r0, inici
	addi	r0, r0, 1
	;; salto mal alineado
	jmp	r0
	halt
