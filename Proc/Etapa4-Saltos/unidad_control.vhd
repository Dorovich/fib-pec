LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.alucodes.all;

ENTITY unidad_control IS
  PORT (boot      : IN  STD_LOGIC;
        clk       : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        op        : OUT aluop;
        wrd       : OUT STD_LOGIC;
        addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        pc        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        ins_dad   : OUT STD_LOGIC;
        in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        immed_x2  : OUT STD_LOGIC;
        wr_m      : OUT STD_LOGIC;
        rb_n	  : OUT STD_LOGIC;
        word_byte : OUT STD_LOGIC;
        aluout    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        z         : IN STD_LOGIC;
        pc_act    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
END unidad_control;


ARCHITECTURE Structure OF unidad_control IS

  -- Aqui iria la declaracion de las entidades que vamos a usar
  -- Tambien crearemos los cables/buses (signals) necesarios para unir las entidades
  -- Aqui iria la definicion del program counter y del registro IR

  COMPONENT control_l IS
    PORT (ir        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op        : OUT aluop;
          ldpc      : OUT STD_LOGIC;
          wrd       : OUT STD_LOGIC;
          addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          wr_m      : OUT STD_LOGIC;
          in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
          immed_x2  : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
          rb_n	    : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT multi IS
    PORT(clk       : IN  STD_LOGIC;
         boot      : IN  STD_LOGIC;
         ldpc_l    : IN  STD_LOGIC;
         wrd_l     : IN  STD_LOGIC;
         wr_m_l    : IN  STD_LOGIC;
         w_b       : IN  STD_LOGIC;
         ldpc      : OUT STD_LOGIC;
         wrd       : OUT STD_LOGIC;
         wr_m      : OUT STD_LOGIC;
         ldir      : OUT STD_LOGIC;
         ins_dad   : OUT STD_LOGIC;
         word_byte : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL new_pc, pcplus2 : std_logic_vector(15 downto 0);
  SIGNAL ir_reg : std_logic_vector(15 downto 0);
  SIGNAL act_pc : std_logic;
  SIGNAL act_ir : std_logic;
  SIGNAL Iop : aluop;
  SIGNAL Iwr_m : std_logic;
  SIGNAL Ildpc : std_logic;
  SIGNAL Iwrd : std_logic;
  SIGNAL Iwb : std_logic;
  SIGNAL tknbr : std_logic_vector(1 downto 0);
BEGIN

  -- Aqui iria la declaracion del "mapeo" (PORT MAP) de los nombres de las entradas/salidas de los componentes
  -- En los esquemas de la documentacion a la instancia de la logica de control le hemos llamado c0
  -- Aqui iria la definicion del comportamiento de la unidad de control y la gestion del PC y del IR

  c0 : control_l PORT MAP(ir => ir_reg,
                          op => Iop,
                          ldpc => Ildpc,
                          wrd => Iwrd,
                          addr_a => addr_a,
                          addr_b => addr_b,
                          addr_d => addr_d,
                          immed => immed,
                          wr_m => Iwr_m,
                          in_d => in_d,
                          immed_x2 => immed_x2,
                          rb_n => rb_n,
                          word_byte => Iwb);
  
  m0 : multi PORT MAP(clk => clk,
                      boot => boot,
                      ldpc_l => Ildpc,
                      wrd_l => Iwrd,
                      wr_m_l => Iwr_m,
                      w_b => Iwb,
                      ldpc => act_pc,
                      wrd => wrd,
                      wr_m => wr_m,
                      ldir => act_ir,
                      ins_dad => ins_dad,
                      word_byte => word_byte);

  op <= Iop;  
  pc <= new_pc;
  pcplus2 <= new_pc + 2;
  pc_act <= pcplus2;

  tknbr <= "01" when (Iop = OPBZ and z = '1') or (Iop = OPBNZ and z = '0') else
           "10" when Iop = OPJMP or Iop = OPJAL or (Iop = OPJZ and z = '1') or (Iop = OPJNZ and z = '0') else
           "00";
  
  PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      if boot = '1' then
        new_pc <= x"C000";
      elsif act_pc = '1' then
        case tknbr is
          when "00" =>
            new_pc <= pcplus2;
          when "01" =>
            new_pc <= pcplus2 + shl(ir_reg(7 downto 0), x"01");
          when "10" =>
            new_pc <= aluout;
          when others =>
            new_pc <= new_pc;
        end case;
      end if;
    end if;
  END PROCESS;

  PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      if act_ir = '1' then
        ir_reg <= datard_m;
      end if;
    end if;
  END PROCESS;
  
END Structure;
