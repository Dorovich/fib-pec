package opcodes is

constant OP_ARITH: std_logic_vector(3 downto 0) := "0000";
constant OP_LD: std_logic_vector(3 downto 0) := "0011";
constant OP_ST: std_logic_vector(3 downto 0) := "0100";
constant OP_LDB: std_logic_vector(3 downto 0) := "1101";
constant OP_STB: std_logic_vector(3 downto 0) := "1110";
constant OP_MOV: std_logic_vector(3 downto 0) := "0101";
constant OP_SPECIAL: std_logic_vector(3 downto 0) := "1111";

end opcodes;